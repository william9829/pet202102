package br.edu.cest.pet.entity.staff;

import java.math.BigDecimal;

import br.edu.cest.pet.entity.EnCategoria;
import br.edu.cest.pet.entity.PessoaFisica;
import br.edu.cest.pet.vo.EnTurno;

public class Funcionario extends PessoaFisica {

    // super - pai
	// TODO - turnos (matutino, noturno, vespertino)
	// TODO - categorias (veterinario - cfmv, estagiário vet - cfmv, estagiário atend)
	
	private EnTurno turno;
	protected EnCategoria categoria;
	private BigDecimal salario;
	private String registro;
	
	Funcionario(String nome, EnTurno turno, String registro) {
		super();
		super.setNome(nome);
		this.turno = turno;
		this.salario = salario;
		this.registro = registro;
	}
	
	public EnCategoria getCategoria() {
		return categoria;
	}
	
	
}
